<?php

class Vehiculo {

    public $matricula;
    private $color;
    protected $encendido;

    function __construct() {
        echo func_num_args();
    }

    public function encender() {
        $this->encendido = true;
        echo 'Vehiculo encendido <br />';
    }

    public function apagar() {
        $this->encendido = false;
        echo 'Vehiculo apagado <br />';
    }

}


