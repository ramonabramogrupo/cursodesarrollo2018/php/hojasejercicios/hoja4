<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        class Usuario {

            var $nombre = "defecto";
            private $edad;
            protected $telefono;

            public function getNombre() {
                return $this->nombre;
            }

            public function getEdad() {
                return $this->edad;
            }

            public function getTelefono() {
                return $this->telefono;
            }

            public function setNombre($nombre) {
                $this->nombre = $nombre;
            }

            public function setEdad($edad) {
                $this->edad = $edad;
            }

            public function setTelefono($telefono) {
                $this->telefono = $telefono;
            }

        }

        $persona = new Usuario();
        echo $persona->nombre;
        $persona->setEdad(51);
        $persona->setTelefono("232323");
        var_dump($persona);
        $persona->nombre = "Silvia";
        $persona->edad=12;
        $persona->telefono="202020";
        var_dump($persona);
        ?>
    </body>
</html>
